# Run Steps

-   clone repo
-   navigate to root directory of repo on your machine
-   run:

```
npm i
```

-   either open the repo in vscode and click the play button (which uses my run configuration and attaches to your default browser's debugger) or run:

```
npm run start
```
