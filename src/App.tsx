import React from 'react';
import FlappyBirdGame from './FlappyBirdGame';

export default function App() {
    return <FlappyBirdGame />;
}
