export default interface IUpdateEveryFrame {
    update: (...args: Array<any>) => void;
}
